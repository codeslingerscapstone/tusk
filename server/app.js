// Libraries and other dependencies
var restify = require('restify'),
    Knex    = require('knex'),
    mkdirp  = require('mkdirp'),
    fs      = require('fs'),
    child   = require('child_process'),
    pg      = require('pg'),
    mv      = require('mv'),
    _       = require('lodash');

// Settings
var port = 7001,
    ip   = '127.0.0.1';

// Database connection information
var knex = Knex.initialize({
    client: 'postgres',
    connection: {
        host:      '127.0.0.1',
        user:      'web',
        password:  '',
        database:  'ivory',
        faulty:    'password',
        charset:   'utf-8'
    }
});

// Create and name the server
var server = restify.createServer({
    name: 'tusk'
});

// Needed in order to parse POST data, like files
server.use(restify.urlEncodedBodyParser({ mapParams : false }));

// All program files should be uploaded via post data
// executionId: executionId of the execution
//     time submitted
//     runtime duration
//     student id (or username?) // TODO: which?
//     professor id (or username?) // TODO: which?
//     problem id (or name?) // TODO: which?
server.post('/run', function (req, res, next) {
    var attemptID = req.body.attemptID,
        mainFile  = req.body.mainFile,
        exID      = req.body.exID,

        assignmentID, userID, programID, userName;

    // Function to start everything off, calls the others
    var getAttempt = knex('AssignmentAttempts').where('id', attemptID);

    // Function to get the Assignment from the Attempt
    var getAssignment = function (resp) {
        assignmentID = resp[0].AssignmentId;
        userID       = resp[0].UserId;

        knex('Assignments').where('id', assignmentID).then(getUser);
    };

    // Function to get the User from the Attempt
    var getUser = function (resp) {
        programID = resp[0].ProgramId;

        knex('Users').where('id', userID).then(writeConfig);
    };

    // Function to write out the config file
    var writeConfig = function (resp) {
        userName  = resp[0].username.toLowerCase();
        var cfgString = 'mainFile="' + mainFile + '"\n',
            M
            cfgPath   = process.env.HOME + '/ivory/tusk/user_folders/'
                        + userName + '/' + programID,
            cfgName   = cfgPath + '/submission.cfg';

        // Make sure the directory exists
        mkdirp.sync(cfgPath);

        // Write the file, set a flag in Executions, and then call Tusk
        fs.writeFile(cfgName, cfgString, function (err) {
            if (err) throw err;
            console.log('Wrote', cfgName);
            callTusk();
        });
    };

    // Function to call Tusk with the right parameters
    var callTusk = function () {
        console.log('calling tusk', userName, programID, exID);
        var tusk = child.spawn('../tusk.sh', [userName, programID, exID]);

        tusk.stdout.on('data', function (data) {
            console.log(data.toString());
        });

        // Wait for Tusk to exit
        tusk.on('close', function (exitCode) {
            console.log('tusk exited with code', exitCode);

            // Set the 'done' flag for the Execution
            knex('Executions').where('id', exID).update({ status: '1' })
            .then(function (resp) {
                knex('Executions').where('id', exID).update({outputFile: 15})
                .then(function(resp) {
                    console.log('there were', resp, 'executions modified');
                });
            });
        });
    };

    // use the information from the program to create a config file for tusk
    getAttempt.then(getAssignment);

    // End the request
    req.end();
});

server.post('/create', function (req, res, next) {
    var programID = req.body.programID;

    var compileCommands = {
        'c++':         'g++',
        'java':        'javac',
        'python':      '',
        'javascript':  ''
    };
    var runCommands = {
        'c++':         '',
        'java':        'java',
        'python':      'python',
        'javascript':  'node'
    };

    knex('Programs').where('id', programID).then(function (resp) {
        var language       = resp[0].language,
            hddSpace       = resp[0].diskSpace,
            memory         = resp[0].memory,
            compileCommand = compileCommands[language],
            runTimeLimit   = resp[0].runLimit,
            runCommand     = runCommands[language],
            hasTestInput   = resp[0].input,
            outputAccuracy = resp[0].accuracy,

            cfgPath   = process.env.HOME + '/ivory/tusk/programs/' + programID,
            cfgName   = cfgPath + '/problem.cfg',
            cfgString = 
                'language="'       + language       + '"\n'  +
                'hddSpace="'       + hddSpace       + '"\n'  +
                'memory="'         + memory         + '"\n'  +
                'compileCommand="' + compileCommand + '"\n'  +
                'runTimeLimit="'   + runTimeLimit   + '"\n'  +
                'runCommand="'     + runCommand     + '"\n'  +
                'hasTestInput='    + hasTestInput   + '\n'   +
                'outputAccuracy="' + outputAccuracy + '"\n';
                

        mkdirp.sync(cfgPath);

        fs.writeFile(cfgName, cfgString, function (err) {
            if (err) throw err;
            console.log('new problem: config file written');
        });
    });

    res.end();
});

// Start the server on the specified port
server.listen(port, ip, function () {
    console.log('Tusk server started on ' + ip + ':' + port);
});
