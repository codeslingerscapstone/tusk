FROM      ubuntu
MAINTAINER Austin Bratcher <austin@austinbratcher.com>

# make sure the package repository is up to date
RUN echo "deb http://archive.ubuntu.com/ubuntu precise main universe" > /etc/apt/sources.list
RUN apt-get update

#ensure ability to add repos
RUN apt-get install -y python-software-properties make

#install g++/gcc
RUN apt-get install -y g++

#install java
RUN add-apt-repository -y ppa:webupd8team/java
RUN apt-get update
RUN echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
RUN echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections
RUN apt-get install -y oracle-java7-installer

#install python
RUN apt-get install -y python

#install node
RUN add-apt-repository -y ppa:chris-lea/node.js && apt-get update
RUN apt-get purge -y python-software-properties python g++ make
RUN apt-get install -y nodejs