#!/bin/bash
#requires that complete command to execute is given in parameters


arg=("$@")
programToRun=`echo "${arg[@]}"`
exportFile="./variables.cfg"

shopt -s nullglob
for input in ./*.in ; do
    input=`basename $input`
    output="`basename $input .in`.res"
    { $programToRun < ./$input || \
    { echo "Your program took too long" \
    && echo "timeLimitExceeded=true" >> $exportFile ; } }  > ./$output
#   cat ./$output
done

echo "All test cases were run"
