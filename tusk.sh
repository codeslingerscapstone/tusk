#!/bin/bash

# Program     : Tusk.sh
# Author      : Austin Bratcher <austin_bratcher@baylor.edu>
# Description : Sandbox that will be used by Ivory LMS. This tool is
#             : dependent on Docker.io. Visit www.docker.io for
#             : documentation and tutorials


# Other tidbits of knowledge to know about Tusk:
# - We use an external processes monitor (user monitor) because Docker creates
# all processes as root, then changes them to the specified user name. PAM
# limits prevent things from starting, rather than killing after starting. 
# - We use the timeout command to monitor the time a program has run
# - We use mount/umount (/mnt) to create fake hard drives for tusk containers
# to use and then drop the fake drives as soon as the script ends. All files
# that are placed in the script are deleted. 






#pass via command line
realUser=$1 
#professor=$2
programId=$2
executionId=$3 #execution id?


SANDBOX="$HOME/ivory/tusk"; #something that will need to be updated before deployment
user=`$SANDBOX/randomString.sh` #random user name to prevent collision in case of failure
userHostDirectory="/mnt/$user" #directory sandbox will access
outputFile="$userHostDirectory/results/$executionId-res.txt"

#Tusk Variables (tuskVariables.cfg)local options
source $SANDBOX/tuskVariables.cfg
#dockerImage='tusk/basic'
#containerDirectory='/home/sandbox'
#dns='--dns=["8.8.8.8","8.8.4.4"]'
#staticSharedFolders='-v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro'
#programToRun=""
#tooManyProc=false
#timeLimitExceeded=false

#variables stored in problem.cfg --> need to make sure each option is 
# properly formatted (i.e. -m 2048m)
#language
#hddSpace of form ="1024M"
#memory of form ="-m 2048m"
#runTimeLimit of form ="60s"
#compileCommand
#runCommand
#hasTestInput
#outputAccuracy

#access configuration from professors code:
#$SANDBOX/$programId is a temporary directory for storing the cfg
mkdir $SANDBOX/"$programId"
wget -q -P $SANDBOX/"$programId" -r --no-host-directories -np \
--reject "index.html*" "$fileServer/programs/$programId/problem.cfg" >> /dev/null
source "$SANDBOX/$programId/programs/$programId/problem.cfg"



#---------------------SETUP AREA-----------------------------------#

#Add user
sudo adduser --conf $SANDBOX/dockerAdduser.conf --disabled-password \
    --no-create-home --gecos "" --ingroup docker "$user" >> /dev/null;

sudo mkdir $userHostDirectory;
sudo mount -o size=$hddSpace -t tmpfs none $userHostDirectory;
sudo chmod 777 $userHostDirectory 

wget -q -P $userHostDirectory -r --no-host-directories -np \
--reject "index.html*" "$fileServer/user_folders/$realUser/$programId/" >> /dev/null
mv $userHostDirectory/user_folders/"$realUser"/"$programId"/* $userHostDirectory
sudo rm -r $userHostDirectory/user_folders
#END ADDUSER



# start monitor for user
# externalVariableFile is used to determine if the code forks too much 
# or runs out of time. 
externalVariableFile="$userHostDirectory/variables.cfg"
touch $externalVariableFile
chmod 777 $externalVariableFile
sudo $SANDBOX/userMonitor.sh $user $externalVariableFile >> /dev/null &
monitor_id=$!;



#-----------------COMPILE/RUN SETUP AREA----------------------------#

#determine if code needs compiled, then prepare to compile and run:

#list all files that are in student directory except .cfg
userCodeFiles=`ls -I *.cfg $userHostDirectory`
shouldCompile=true
shouldRun=true
if [[ $language == "java" ]]; then
	if [[ $userCodeFiles == *.jar ]]; then
		#user is running a jar file
		programToRun=$runCommand" -jar "$userCodeFiles
		shouldCompile=false
	else 
		# user is running a plain java file
		# get name of file without extension attached
		programToRun=`ls -I *.cfg -I *.class $userHostDirectory | sed -e 's/\..*$//'` 
		programToRun=$runCommand" "$programToRun
	fi
elif [[ $language == "c++" ]]; then
	# c++ file
	userCodeFiles="*.cpp"
	programToRun="./a.out"
else
	#we are python or javascript
	#access configuration from student submission:
	#will change this line to reflect pull from database 
	#What we get access to from this file:
	#mainFile
	source "$userHostDirectory/submission.cfg"
	shouldCompile=false
	programToRun=$runCommand" ./"$mainFile
fi

#create output file
mkdir $userHostDirectory/results
touch $outputFile
echo "Execution ID :" "$executionId" >> $outputFile;
echo "Student      :" "$realUser" >> $outputFile;
#echo "Professor    :" "$professor" >> $outputFile;
echo "Problem      :" "$programId" >> $outputFile;
echo "Date         :" `date` >> $outputFile;
printf "\n\n" >> $outputFile;

#-------------------COMPILING AREA----------------------------------#

if $shouldCompile ; then
	# create container and run compile command in it.
	compileOutput=$(sudo -u $user \
	$docker run -t $dns $staticSharedFolders \
	-v $userHostDirectory:$containerDirectory:rw \
	--workdir="$containerDirectory" $remove \
	--user="$user" "$dockerImage" sh -c "$compileCommand $userCodeFiles");
    
    #Determine if code compiled successfully 
    exitStatus=$?
    if [ $exitStatus -ne 0 ]; then
    	echo "Compile Status: Program failed to compile" >> $outputFile
    	shouldRun=false
    else
    	echo "Compile Status: Program compiled successfully" >> $outputFile
    fi
    printf "Compile output:\n $compileOutput" >> $outputFile;
    printf "\n\n" >> $outputFile;

else
	echo "Compile Status: Did not attempt to compile" >> $outputFile
	echo "Compile Output: Did not attempt to compile" >> $outputFile
fi

#--------------------RUNNING AREA-------------------------------------#


# name new container after user 

# sudo -u $user --> we can add this so that the container will run under the 
# student user, but when procMonitor sees this it will kill the shell this is
# running in. I'd rather let timeout handle killing this and then have some flag
# set to pick up on forkbombs. 

# - Consider adding option in timeout command to specify SIGKILL signal; 
# SIGTERM can be intercepted by the processes (i believe, thought I don't
# know how this works with DOCKER) and could prevent stopping the process

if $shouldRun ; then

	echo "Run Status: Program was run" >> $outputFile

	if $hasTestInput ; then 

		# copy test data into user's directory
		wget -q -P $userHostDirectory -r --no-host-directories \
		-np --reject "index.html*" \
		"$fileServer/programs/$programId/data/" >> /dev/null ;
		#move files up to the correct place
		mv $userHostDirectory/programs/$programId/data/* $userHostDirectory

		if [[ $outputAccuracy == "low" ]]; then
			#loop all test cases inside a single container

			echo "Run Output: " >> $outputFile
			programToRun="./test_all.sh timeout $runTimeLimit $programToRun"

			#copy test_all script to user directory for low accuracy testing
			cp $SANDBOX/test_all.sh $userHostDirectory

			# run test_all script 
			{ sudo -u $user $docker run -t "$dns" $memory $staticSharedFolders \
			-v $userHostDirectory:$containerDirectory:rw \
			--workdir="$containerDirectory" $remove --name="$user" \
			--user="$user" "$dockerImage" sh -c "$programToRun" ; } >> $outputFile

        else
			echo "Run output: All tests run individually" >> $outputFile
			# create new container for each test

			shopt -s nullglob #watch out for no .in files
			for input in $userHostDirectory/*.in ; do
				input=`basename $input` #input file
				output="`basename $input .in`.res" #output file

				# run test inside container 
				{ timeout $runTimeLimit \
				sudo -u $user $docker run -t "$dns" $memory $staticSharedFolders \
				-v $userHostDirectory:$containerDirectory:rw \
				--workdir="$containerDirectory" --name="$user" \
				--user="$user" "$dockerImage" sh -c "$programToRun < ./$input > ./$output" || \
				{ sudo $docker kill $user >> /dev/null && timeLimitExceeded=true && \
				echo Your program took too long; } } > $userHostDirectory/results/$output ;

				sudo $docker kill $user >> /dev/null
				sudo $docker rm $user >> /dev/null
			done
		fi
	else
		# no test input
		# future addition: add ability to gather output and test without
		# some input file. 
		echo "Run output: No input given. Output as follows:" >> $outputFile
		{ timeout $runTimeLimit \
		sudo -u $user $docker run -t "$dns" $memory $staticSharedFolders \
		-v $userHostDirectory:$containerDirectory:rw \
		--workdir="$containerDirectory" --name="$user" \
		--user="$user" "$dockerImage" sh -c "$programToRun" || \
		{ sudo $docker kill $user>> /dev/null && timeLimitExceeded=true && \
		echo Your program took too long; } } >> $outputFile ;

		
	fi

	# remove container if leftover and clean up Processes
	# still producing some unexpected output
	sudo $docker kill $user >> /dev/null
	sudo $docker rm $user >> /dev/null
	sudo killall --user $user >> /dev/null; 
else
	echo "Run Status: Program was not run" >> $outputFile
	echo "Run Output: Program was not run" >> $outputFile
fi 

#----------------------OUTPUT TEST AREA-------------------------------#

if $shouldRun ; then
	if $hasTestInput ; then

		source $externalVariableFile #access variables
		allTestPassed=true

		for answer in $userHostDirectory/*.ans ; do
			result=`basename $answer .ans`.res
			diff -N $userHostDirectory/$result $answer >> /dev/null
			passed="$?" #must be run immediatly after diff command to get return code

			if [[ "$passed" == "0" ]] ; then
				echo "Test for "`basename -s .res $result`" passed" >> $outputFile
				#echo "Output as follows:" >> $outputFile
				#cat $userHostDirectory/$result >> $outputFile
			else
				allTestPassed=false
				echo "Test for "`basename -s .res $result`" failed" >> $outputFile
			fi
		done

		printf "\n\n" >> $outputFile;
		if $tooManyProc ; then
			echo "Too many processes created" >> $outputFile
			allTestPassed=false
		elif $timeLimitExceeded ; then
			echo "Your program exceeded the allowed time to run" >> $outputFile
			allTestPassed=false
		fi

		if $allTestPassed ; then
			echo "All tests were passed" >> $outputFile
		else
			echo "Not all tests were passed" >> $outputFile
		fi
	else
		printf "\n\n" >> $outputFile;
		if $tooManyProc ; then
			echo "Too many processes created" >> $outputFile
		elif $timeLimitExceeded ; then
			echo "Your program exceeded the allowed time to run" >> $outputFile
		fi
	fi
else
	echo "Test status: No tests were run" >> $outputFile
fi
#----------------------CLEAN UP AREA ---------------------------------#

sudo kill $monitor_id >> /dev/null;
rm $externalVariableFile


# copy results from userHostDirectory
if [ ! -d "$SANDBOX/user_folders/$realUser/results/" ]; then
	mkdir $SANDBOX/user_folders/$realUser/results/ >> /dev/null
fi 
sudo mv $outputFile $SANDBOX/user_folders/$realUser/results/

if [ ! -d "$SANDBOX/user_folders/$realUser/old_submissions/" ]; then
	mkdir $SANDBOX/user_folders/$realUser/old_submissions/ >> /dev/null
fi 

mkdir $SANDBOX/user_folders/$realUser/old_submissions/$executionId
sudo mv $userHostDirectory/* $SANDBOX/user_folders/$realUser/old_submissions/$executionId/
shopt -s nullglob #watch out for no .in files
sudo rm $SANDBOX/user_folders/$realUser/old_submissions/$executionId/*.in
shopt -s nullglob #watch out for no .in files
sudo rm $SANDBOX/user_folders/$realUser/old_submissions/$executionId/*.ans
shopt -s nullglob #watch out for no .in files
sudo rm $SANDBOX/user_folders/$realUser/$programId/*
#sudo mv $SANDBOX/user_folders/$realUser/$programId/* $SANDBOX/user_folders/$realUser/old_submissions/$executionId/
# consider creating a submissions folder and moving submitted code there

#Remove user
sudo rm -r $userHostDirectory/*
sudo umount $userHostDirectory; #-n option
sudo rmdir $userHostDirectory;

sudo deluser --quiet --conf $SANDBOX/dockerDeluser.conf $user >> /dev/null;
sudo rm -r $SANDBOX/$programId
#END remove user
