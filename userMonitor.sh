#!/bin/bash

user=$1
exportFile=$2

while [ true ]
do
    userID=$(id -u $user);
    numProc=$( ps -u $userID | wc -l); 

    if [ $numProc -gt 75 ]
    then
        echo "tooManyProc=true" >> $exportFile
        sudo killall --user $user >> /dev/null;
    fi 
done
